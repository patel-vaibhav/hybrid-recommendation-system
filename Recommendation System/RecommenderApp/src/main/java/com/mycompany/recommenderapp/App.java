/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.recommenderapp;

import java.io.File;
import java.io.IOException;
import java.util.List;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.UserBasedRecommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;

/**
 *
 * @author aakanxu
 */
public class App {
    public static void main(String[] args) throws IOException, TasteException {
        
        DataModel model = new FileDataModel(new File("data/dataset.csv"));
        
        UserSimilarity similarity = new PearsonCorrelationSimilarity(model);
       // UserSimilarity userSimilarity = new PearsonCorrelationSimilarity(model, org.apache.mahout.cf.taste.common.Weighting.WEIGHTED);
        UserNeighborhood neighborhood = new ThresholdUserNeighborhood(0.1, similarity, model);
        UserBasedRecommender recommender = new GenericUserBasedRecommender(model, neighborhood, similarity);
        //UserBasedRecommender recommender = new 
        List<RecommendedItem> recommendations = recommender.recommend(2, 2);
for (RecommendedItem recommendation : recommendations) {
  System.out.println(recommendation);
  //  System.err.println("user similarity: "+userSimilarity);
    
}
    }
}
