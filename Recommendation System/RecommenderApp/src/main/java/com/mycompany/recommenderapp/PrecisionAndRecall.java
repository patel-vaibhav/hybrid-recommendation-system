/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.recommenderapp;

/**
 *
 * @author aakanxu
 */
import java.io.*;
 
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.eval.IRStatistics;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.eval.RecommenderIRStatsEvaluator;
import org.apache.mahout.cf.taste.impl.eval.GenericRecommenderIRStatsEvaluator;
import org.apache.mahout.cf.taste.impl.model.file.*;
import org.apache.mahout.cf.taste.impl.neighborhood.*;
import org.apache.mahout.cf.taste.impl.recommender.*;
import org.apache.mahout.cf.taste.impl.similarity.*;
import org.apache.mahout.cf.taste.model.*;
import org.apache.mahout.cf.taste.neighborhood.*;
import org.apache.mahout.cf.taste.recommender.*;
import org.apache.mahout.cf.taste.similarity.*;
import org.apache.mahout.common.RandomUtils;
 
public class PrecisionAndRecall {
 
  public static void main(String[] args) throws Exception {
      // Create a data source from the CSV file
      File userPreferencesFile = new File("data/movies.csv");
      RandomUtils.useTestSeed();
       
      DataModel dataModel = new FileDataModel(userPreferencesFile);
       
      RecommenderIRStatsEvaluator recommenderEvaluator = new GenericRecommenderIRStatsEvaluator();
           
      RecommenderBuilder recommenderBuilder = new RecommenderBuilder() {
          @Override
          public Recommender buildRecommender(DataModel dataModel) throws TasteException {
              UserSimilarity userSimilarity = new PearsonCorrelationSimilarity(dataModel);
              UserNeighborhood userNeighborhood = new NearestNUserNeighborhood(10, userSimilarity, dataModel);
 
              // Return a new instance of a generic user based recommender with the dataModel, the userNeighborhood and the userSimilarity
              return new GenericUserBasedRecommender(dataModel, userNeighborhood, userSimilarity);
          }
      };
       
      IRStatistics statistics = 
              recommenderEvaluator.evaluate(
                      recommenderBuilder, null, dataModel, 
                      null, 2, GenericRecommenderIRStatsEvaluator.CHOOSE_THRESHOLD, 1.0);
      System.out.println("---------------------------------------------------");
      System.out.println("");
      System.out.println("");
      System.out.format("The recommender precision is %f%n", statistics.getPrecision());
      System.out.format("The recommender recall is %f%n", statistics.getRecall());
      System.out.format("The recommender fall-out is %f%n", statistics.getFallOut());
      //System.out.format("The recommender recall is %f%n", statistics.getF1Measure());
      System.out.println("");
      System.out.println("");
  }
}